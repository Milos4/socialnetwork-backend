package controllers


import models.{Account, Post}

import play.api.mvc._
import slick.driver.H2Driver.api._
import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class LoginController @Inject()(val controllerComponents: ControllerComponents)(implicit ec: ExecutionContext)
  extends BaseController  {

  val db = Database.forURL("jdbc:mysql://localhost:3306/socialnetwork", driver = "com.mysql.cj.jdbc.Driver" , password = "root" , user = "root")


  def login = Action {
    Ok(views.html.login())
  }

  var accounts1: Array[Account] = new Array[Account](5)
  var account = new Account(1, "marko", "pass", "Marko", "Markovic", "marko@gmail.com", "064312321", "picture")

  var post1 = new Post(1, 1, "Cao", null)
  var post2 = new Post(2, 1, "Cao sta ima", null)

  var posts1: Array[Post] = Array(post1, post2)


  def validateLogin() = Action { request =>
    var x = 0
    val postVal = request.body.asFormUrlEncoded
    postVal.map { args =>
      val username1 = args("username").head
      val password = args("password").head

      if (username1 == username1) {
        Redirect(routes.LoginController.login()).withSession("username" -> username1)
      } else {
        Ok("dadasdas")

      }
    }.getOrElse(Ok("Ops"))
  }

    /*def validateLogin1(name: String , pass: String) = Action.async { implicit request =>
        val resultingUsers: Future[Seq[Account]] = db.run(accounts.filter(_.username === name).result)
        resultingUsers.map(users => Redirect(routes.LoginController.listAll()))
      }*/


  def createAccount() = Action { request =>
    val postVal = request.body.asFormUrlEncoded
    postVal.map { args =>
      val username = args("username").head
      val password = args("password").head
      val fristName = args("fristName").head
      val lastName = args("lastName").head
      val email = args("email").head
      val phone = args("phone").head
      val newaccount = new Account(1, username, password, fristName, lastName, email, phone, "")
      if (accounts1 != null) {
        accounts1 = accounts1 :+ newaccount
        Redirect(routes.LoginController.login())
      } else {
        Redirect(routes.LoginController.login())
        Ok("aaaaaa")
      }
    }.getOrElse(Ok("Ops"))
  }
   def loginS = Action{ request =>
    var posts1: Array[Post] = Array()
    val accountSe = request.session.get("username").map { username =>
      for (post <- posts1) {
        if (post.id == username) {
          posts1 = posts1 :+ post;
        }
      }
    }
    Ok(views.html.home(posts1))
  }
}
