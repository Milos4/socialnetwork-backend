package controllers

import dto.{FriendshipDto}
import models.{Account, Friendship, FriendshipForm, Like, LikeForm}
import play.api.libs.json.Json
import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents, Request}
import service.FriendshipService

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class FriendshipController  @Inject()(val controllerComponents: ControllerComponents , friendshipService: FriendshipService) (implicit ec: ExecutionContext)
  extends BaseController{

  def listAll: Action[AnyContent] = Action.async { implicit request =>
    val futureFriendship = friendshipService.findAll()
    futureFriendship.map(friendship => Ok(Json.toJson(friendship)))
  }

  def getById(id: Int) = Action.async { implicit request: Request[AnyContent] =>
    friendshipService.findById(id) map { fs =>
      Ok(Json.toJson(fs))
    }
  }

  /*ef getByReceiverId(id: Int) = Action.async { implicit request: Request[AnyContent] =>
    friendshipService.findByReciverId(id) map { fs =>
      var fsDto : Seq [FriendshipDto] = Seq[FriendshipDto]()
      for (friend <- fs) {
        var friendDto = new FriendshipDto(friend._1,friend._2,friend._3,friend._4, friend._5, friend._6);
        fsDto = fsDto :+ friendDto
      }
      Ok(Json.toJson(fsDto))
    }
  }*/

  def getByReceiverId1(id: Int) = Action.async { implicit request: Request[AnyContent] =>
    friendshipService.findByReciverId(id) map { fs =>
      var fsDto : Seq [FriendshipDto] = Seq[FriendshipDto]()
      for (friend <- fs) {
        var friendDto = new FriendshipDto(friend._1,friend._2,friend._3,friend._4, friend._5 , friend._6);
        fsDto = fsDto :+ friendDto
      }
      Ok(Json.toJson(fsDto))
    }
  }


  /*def add() = Action.async { implicit request: Request[AnyContent] =>
    FriendshipForm.form.bindFromRequest.fold(
      // if any error in submitted data
      errorForm => {
        errorForm.errors.foreach(println)
        Future.successful(BadRequest("Error!"))
      },
      data => {
        val newFS = Friendship(data.id, data.senderId, data.receiverId,data.status )
        friendshipService.addFriendShip(newFS).map( _ => Redirect(routes.FriendshipController.listAll()))
      })
  }*/


  def add() = Action.async { implicit request: Request[AnyContent] =>
    val friendship = request.body.asJson.get.as[Friendship]
    friendshipService.exists(friendship.senderId,friendship.receiverId).flatMap{
      case false =>
        friendshipService.addFriendShip(friendship).map{fs =>
          Ok(Json.toJson(fs))}
      case true =>
    Future(BadRequest("Friendship already exists"))
    }.recover {
    case ex => InternalServerError(ex.getMessage)
  }
  }


 /* def update(id: Int) = Action.async { implicit request: Request[AnyContent] =>
    FriendshipForm.form.bindFromRequest.fold(
      // if any error in submitted data
      errorForm => {
        errorForm.errors.foreach(println)
        Future.successful(BadRequest("Error!"))
      },
      data => {
        val fs = Friendship(id, data.senderId , data.receiverId , data.status)
        friendshipService.updateFriendShip(fs).map( _ => Redirect(routes.FriendshipController.listAll()))
      })
  }*/

  def update(id: Int) = Action.async { implicit request: Request[AnyContent] =>
    val friendship = request.body.asJson.get.as[Friendship]
    friendshipService.updateFriendShip(friendship).map{ _ =>
      Ok(Json.toJson(friendship))
    }
  }

  def exist(id: Int, id2 : Int) = Action.async { implicit request: Request[AnyContent] =>
      friendshipService.exists(id,id2).map{ a =>
      Ok(Json.toJson(a.&&(true)))}
    }


  def delete(id: Int) = Action.async { implicit request: Request[AnyContent] =>
    friendshipService.deleteFriendship(id) map { { res =>
      Ok(Json.toJson("Success delete "))}
    }

  }


}
