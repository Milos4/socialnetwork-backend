package controllers

import db.DatabaseSchema
import models.{Friendship, Like}
import play.api.libs.json.Json
import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents, Request}
import service.LikeService

import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class LikeController @Inject()(val controllerComponents: ControllerComponents , likeService: LikeService) (implicit ec: ExecutionContext)
  extends BaseController{


  def listAll: Action[AnyContent] = Action.async { implicit request =>
    val futureAccounts = likeService.findAll()
    futureAccounts.map(acc => Ok(Json.toJson(acc)))
  }

  def getById(id: Int) = Action.async { implicit request: Request[AnyContent] =>
    likeService.findById(id) map { post =>
      Ok(Json.toJson(post))
    }
  }

  def add() = Action.async { implicit request: Request[AnyContent] =>
    val like = request.body.asJson.get.as[Like]
    likeService.addLike(like).map{ like =>
      Ok(Json.toJson(like))
    }
  }

  def delete(id: Int) = Action.async { implicit request: Request[AnyContent] =>
    likeService.deleteLike(id) map { { res =>
      Ok(Json.toJson("Success delete :"))}
    }
    }

    def deleteAcc(postId: Int,accId: Int) = Action.async { implicit request: Request[AnyContent] =>
      likeService.deleteLikeAcc(postId,accId) map { { res =>
        Ok(Json.toJson("Success delete :"))}
      }

  }



}
