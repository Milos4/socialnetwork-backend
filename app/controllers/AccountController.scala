package controllers

import dto.{AccountDto, AccountSingIn}
import org.mindrot.jbcrypt.BCrypt
import models.{Account, AccountForm}
import play.api.libs.json.Json
import play.api.mvc.Results.Ok
import service.AccountService
import play.api.mvc._

import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class AccountController @Inject()(accService: AccountService, val controllerComponents: ControllerComponents)(implicit ec: ExecutionContext) extends BaseController {

  def listAll: Action[AnyContent] = Action.async { implicit request =>
    accService.findAll() map { accounts =>
      var accountsDto: Seq[AccountDto] = Seq[AccountDto]()
      for (acc <- accounts) {
        var accDto = AccountDto.acc2AccDto(acc)
        accountsDto = accountsDto :+ accDto
      }
      Ok(Json.toJson(accountsDto))
    }
  }
  //BCrypt.checkpw(password, passwordHash)
  def validateLogin()= Action.async { implicit request =>
    val accSingin = request.body.asJson.get.as[AccountSingIn]
    accService.validateAcc(accSingin.username, accSingin.password) map { acc =>
      var accDto = AccountDto.acc2AccDto(acc)
      Ok(Json.toJson(accDto))
    }
  }

  def getAccById(id: Int) = Action.async { implicit request: Request[AnyContent] =>
    accService.findById(id) map { acc =>
      var accDto = AccountDto.acc2AccDto(acc)
      Ok(Json.toJson(accDto))
    }
  }

  /* def add() = Action.async { implicit request: Request[AnyContent] =>
    AccountForm.form.bindFromRequest.fold(
      // if any error in submitted data
      errorForm => {
        errorForm.errors.foreach(println)
        Future.successful(BadRequest("Error!"))
      },
      data => {
        val newAccount = Account(data.id, data.username, data.password , data.firstName , data.lastName , data.email , data.phone , data.picture)
        accService.addAccount(newAccount).map( _ => Redirect(routes.AccountController.listAll()))
      })
  }*/
/*
  //  Verify passwords
  //val a = BCrypt.checkpw(account.password, passwordHash)
  def add() = Action.async { implicit request: Request[AnyContent] =>
    val account = request.body.asJson.get.as[Account]
    val newAcc = accService.newAcc(account)
    accService.exists(newAcc.username).map { result =>
      if (!result) {
        accService.addAccount(newAcc).map(_ => Redirect(routes.AccountController.listAll()))
      }else{
        Future(BadRequest("Username postoji"))
      }
    }
    Future(Redirect(routes.AccountController.listAll()))
  } */


  def add() = Action.async { implicit request: Request[AnyContent] =>
    val account = request.body.asJson.get.as[Account]
    val newAcc = accService.newAcc(account)
    accService.exists(newAcc.username).flatMap {
      case false =>
        accService.addAccount(newAcc).map{acc =>
          var accDto = AccountDto.acc2AccDto(acc)
          Ok(Json.toJson(accDto))}
      case true =>
        Future(BadRequest("Username already exists"))
    }.recover {
      case ex => InternalServerError(ex.getMessage)
    }
  }


/*  def update(id: Int) = Action.async { implicit request: Request[AnyContent] =>
    AccountForm.form.bindFromRequest.fold(
      // if any error in submitted data
      errorForm => {
        errorForm.errors.foreach(println)
        Future.successful(BadRequest("Error!"))
      },
      data => {
        val account = Account(id, data.username, data.password , data.firstName , data.lastName , data.email, data.phone, data.picture)
        accService.updateAccount(account).map( _ => Redirect(routes.AccountController.listAll()))
      })
  }*/

  def update(id: Int) = Action.async { implicit request: Request[AnyContent] =>
    val account = request.body.asJson.get.as[Account]
    accService.updateAccount(account).map{ _ =>
      var accDto = AccountDto.acc2AccDto(account)
      Ok(Json.toJson(accDto))
    }
  }

  def delete(id: Int) = Action.async { implicit request: Request[AnyContent] =>
    accService.deleteAccount(id) map { res =>
      Ok(Json.toJson("Success delete "))}
    }

}
