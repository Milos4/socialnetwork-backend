package controllers

import db.DatabaseSchema
import dto.{PostDto, PostsDto}
import models.{Friendship, Post}
import play.api.libs.json.Json
import play.api.libs.json.OFormat.oFormatFromReadsAndOWrites
import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents, Request}
import repository.PostRepository
import service.{FriendshipService, LikeService, PostService}

import java.sql.Timestamp
import java.time.LocalDateTime
import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class PostController @Inject()(val controllerComponents: ControllerComponents , postService: PostService , friendService : FriendshipService , likeService : LikeService)(implicit ec: ExecutionContext)
  extends BaseController {

  def listAll: Action[AnyContent] = Action.async { implicit request =>
    postService.findAll() map {posts =>
      var postsDto : Seq [PostDto] = Seq[PostDto]()
      for ( post1 <- posts ){
        var postDto = PostDto.postToPostDto(post1)
        postsDto  = postsDto :+ postDto
      }
      Ok(Json.toJson(postsDto))
    }
  }

  def getPostById(id: Int) = Action.async { implicit request: Request[AnyContent] =>
    postService.findById(id) map { post =>
      var postDto = PostDto.postToPostDto(post)
      Ok(Json.toJson(postDto))
    }
  }
  def getPostByAccId(id: Int) = Action.async { implicit request: Request[AnyContent] =>
    postService.findByAccId(id) map { posts =>
      var postsDto : Seq [PostsDto] = Seq[PostsDto]()
      for ( post1 <- posts ){
        var postDto = new PostsDto(post1._1 ,post1._2 , post1._3.toLocalDateTime , post1._4 , post1._5 , post1._8 , post1._9)
        postsDto  = postsDto :+ postDto
      }
      Ok(Json.toJson(postsDto))
    }
  }

  def getPostsForAcc(accId : Int ) =  Action.async { implicit request: Request[AnyContent] =>
   postService.findPostsForAcc(accId).map{ acc =>
     var postsDto : Seq[PostsDto] = Seq[PostsDto]()
     for (account <- acc){
       var postDto = new PostsDto(account._1 ,account._2 , account._3.toLocalDateTime , account._4 , account._5 , account._8 , account._9)
       postsDto  = postsDto :+ postDto
     }
    Ok(Json.toJson(postsDto))
    }
  }

 /* def add() = Action.async { implicit request: Request[AnyContent] =>
    PostForm.form.bindFromRequest.fold(
      // if any error in submitted data
      errorForm => {
        errorForm.errors.foreach(println)
        Future.successful(BadRequest("Error!"))
      },
      data => {
        val newPost = Post(data.id, data.accountId, data.content , data.time )
        postService.addPost(newPost).map( _ => Redirect(routes.PostController.listAll()))
      })
  }*/

  def add() = Action.async { implicit request: Request[AnyContent] =>
    val post1 = request.body.asJson.get.as[PostDto]
    val now = LocalDateTime.now()
    val timeNow = Timestamp.valueOf(now)
    val post = new Post(post1.id , post1.accountId , post1.content , timeNow)
    postService.addPost(post).map{ post =>
      var postDto = PostDto.postToPostDto(post)
      Ok(Json.toJson(postDto))
    }
    }
 /* def update(id: Int) = Action.async { implicit request: Request[AnyContent] =>
    PostForm.form.bindFromRequest.fold(
      // if any error in submitted data
      errorForm => {
        errorForm.errors.foreach(println)
        Future.successful(BadRequest("Error!"))
      },
      data => {
        val post = Post(id, data.accountId , data.content , data.time)
        postService.updatePost(post).map( _ => Redirect(routes.PostController.listAll()))
      })
  }*/

  def update(id: Int) = Action.async { implicit request: Request[AnyContent] =>
    val post = request.body.asJson.get.as[Post]
    postService.updatePost(post).map{ _ =>
        Ok(Json.toJson(post))
    }

  }

  def delete(id: Int) = Action.async { implicit request: Request[AnyContent] =>
    postService.deletePost(id) map { { res =>
      Ok(Json.toJson("Success delete "))}
    }
  }

}
