package models

import play.api.data.Form
import play.api.data.Forms.{mapping, number}
import play.api.libs.json.Json

case class Like(var id : Int,var accountId: Int, var postId: Int)

object Like {
  implicit val likeFormat = Json.format[Like]
}

object LikeForm {
  val form = Form(
    mapping(
      "id" -> number,
      "accountId" -> number,
      "postId" -> number,
    )(Like.apply)(Like.unapply)
  )
}



