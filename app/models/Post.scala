package models
import play.api.libs.json.Json._
import play.api.data.Form
import play.api.data.Forms.{date, localDate, localDateTime, mapping, nonEmptyText, number, sqlTimestamp}
import play.api.libs.json.Json.{fromJson, toJson}
import play.api.libs.json.{Json, Reads, Writes}

import java.sql.Timestamp

import play.api.libs.json._
import java.time.LocalDateTime

case class Post(var id: Int,var accountId : Int ,var content: String, var time : Timestamp)


object Post{

  implicit val timestampReads: Reads[Timestamp] = {
    implicitly[Reads[Long]].map(new Timestamp(_))
  }
  implicit val timestampWrites: Writes[Timestamp] = {
    implicitly[Writes[Long]].contramap(_.getTime)
  }
  implicit val postFormat = Json.format[Post]

}

