package models

import play.api.data.Form
import play.api.data.Forms.{mapping, nonEmptyText, number}
import play.api.libs.json.Json


case class Account(var id : Int,var username: String, var password: String, var firstName: String, var lastName: String, var email: String, var phone: String, var picture: String)

object Account {
  implicit val accountFormat = Json.format[Account]
}

object AccountForm {
  val form = Form(
    mapping(
      "id" -> number,
      "username" -> nonEmptyText,
      "password" -> nonEmptyText,
      "firstName" -> nonEmptyText,
      "lastName" -> nonEmptyText,
      "email" -> nonEmptyText,
      "phone" -> nonEmptyText,
      "picture" -> nonEmptyText,

    )(Account.apply)(Account.unapply)
  )
}


