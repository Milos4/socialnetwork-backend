package models

import play.api.data.Form
import play.api.data.Forms.{mapping, nonEmptyText, number}
import play.api.libs.json.Json

case class Friendship(var id : Int, var senderId : Int, receiverId : Int, status : String)

object Friendship {
  implicit val friendshipFormat = Json.format[Friendship]
}

object FriendshipForm {
  val form = Form(
    mapping(
      "id" -> number,
      "senderId" -> number,
      "receiverId" -> number,
      "status" -> nonEmptyText,
    )(Friendship.apply)(Friendship.unapply)
  )
}