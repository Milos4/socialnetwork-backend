package service

import db.DatabaseSchema
import models._
import repository.{ FriendshipRepository}
import slick.driver.H2Driver.api._

import javax.inject.Inject
import scala.concurrent.Future

class FriendshipService  @Inject()(val friendshipRepository: FriendshipRepository ){

  val db = Database.forURL("jdbc:mysql://localhost:3306/socialnetwork", driver = "com.mysql.cj.jdbc.Driver")

  def findAll()  ={
    friendshipRepository.getAll()
  }

  def findById(id : Int)  ={
    friendshipRepository.getById(id)
  }

  def findBySenderId(id : Int):Future[Seq[Friendship]]  = {
    friendshipRepository.getBySender(id)
  }

  def findByReciverId(id : Int)  = {
    friendshipRepository.getByReceiver(id)
  }

  def findAllFriends( idAcc : Int) = {
    friendshipRepository.getFriendshipsForAcc(idAcc)
  }


  def exists (sender:Int, receiver: Int ) = {
    friendshipRepository.exist(sender,receiver)
  }

  def addFriendShip(friendship: Friendship) = {
    friendshipRepository.add(friendship)
  }

  def updateFriendShip(friendship: Friendship): Future[Int] = {
    friendshipRepository.update(friendship )
  }

  def deleteFriendship(id: Int): Future[Int] = {
    friendshipRepository.delete(id)
  }


}
