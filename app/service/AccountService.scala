package service

import db.DatabaseSchema
import dto.AccountDto
import models.{Account, Post}
import org.mindrot.jbcrypt.BCrypt
import repository.AccountRepository
import slick.driver.H2Driver.api._

import javax.inject.{Inject, Singleton}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Singleton
class AccountService @Inject()(val accountRep : AccountRepository){


  def findAll(): Future[Seq[Account]] = {
    accountRep.getAll()
  }

  def exists(username: String): Future[Boolean] = {
    accountRep.exists(username)
  }

  def newAcc(acc : Account): Account ={
    val passwordHash = BCrypt.hashpw(acc.password, BCrypt.gensalt)
    val newAcc = acc.copy(password=passwordHash)
    newAcc
  }

  def findById(id: Int): Future[Account] = {
    accountRep.getById(id)
  }

  def addAccount(account: Account) = {
    accountRep.add(account)
  }

  def updateAccount(account: Account): Future[Int] = {
    accountRep.update(account )
  }

  def deleteAccount(id: Int): Future[Int] = {
    accountRep.delete(id)
  }

  def validateAcc(username : String , password  : String) : Future[Account] = {
    accountRep.validateLogin(username , password)
  }

}
