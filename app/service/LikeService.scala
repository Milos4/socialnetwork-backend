package service

import db.DatabaseSchema
import models._
import repository.{LikeRepository, PostRepository}
import slick.driver.H2Driver.api._

import javax.inject.Inject
import scala.concurrent.Future

class LikeService  @Inject()(val likeRep : LikeRepository) {

  val db = Database.forURL("jdbc:mysql://localhost:3306/socialnetwork", driver = "com.mysql.cj.jdbc.Driver")

  def findAll()  ={
    likeRep.getAll()
  }

  def findById(id : Int)  ={
    likeRep.getById(id)
  }

  def addLike(like: Like) = {
    likeRep.add(like)
  }

  def updateLike(like: Like): Future[Int] = {
    likeRep.update(like )
  }

  def deleteLike(id: Int): Future[Int] = {
    likeRep.delete(id)
  }

  def deleteLikeAcc(postId: Int,accId: Int): Future[Int] = {
    likeRep.deleteAcc(postId,accId)
  }

}
