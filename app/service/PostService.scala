package service

import db.DatabaseSchema
import models._
import repository.PostRepository
import slick.driver.H2Driver.api._

import javax.inject.Inject
import scala.concurrent.Future

class PostService  @Inject()(val postRep : PostRepository) {

  val db = Database.forURL("jdbc:mysql://localhost:3306/socialnetwork", driver = "com.mysql.cj.jdbc.Driver")

  def findAll()  ={
    postRep.getAll()
  }
  def findById(id : Int): Future[Post]  ={
    postRep.getById(id)
  }

  def findByAccId(id : Int)  ={
    postRep.getByAccountId(id)
  }

  def findPostsForAcc(id : Int) = {
    postRep.getPostsByAccount(id)
  }

  def addPost(post: Post) = {
    postRep.add(post)
  }

  def updatePost(post: Post): Future[Int] = {
    postRep.update(post)
  }

  def deletePost(id: Int): Future[Int] = {
    postRep.delete(id)
  }

}
