package dto



import models.Post
import play.api.libs.json.Json

import java.sql.Timestamp
import java.time.LocalDateTime

case class PostDto(var id: Int,var accountId : Int ,var content: String , var time : LocalDateTime)



object PostDto {
  implicit def accWriter = Json.writes[AccountDto]

  implicit val postFormat = Json.format[PostDto]

  implicit def postToPostDto(post: Post): PostDto = PostDto.this (post.id, post.accountId, post.content , post.time.toLocalDateTime)
}

