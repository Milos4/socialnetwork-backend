package dto

import models.{Account, Post}
import play.api.libs.json.Json

import java.sql.Timestamp
import java.time.LocalDateTime

case class PostsDto(var id: Int,var content : String ,var time : LocalDateTime , var  accId : Int , var username : String , var likes : Int , var isLike : Boolean )

object PostsDto {
  implicit def accWriter = Json.writes[PostsDto]

  implicit val postFormat = Json.format[PostsDto]

}

