package dto

import play.api.libs.json.Json

case class FriendshipDto(var id: Int , var senderId : Int , var  receiverId : Int , var usernameSender : String , var usernameSenderID : Int ,var status : String )

object FriendshipDto {
  implicit def accWriter = Json.writes[FriendshipDto]

  implicit val friendshipDtoFormat = Json.format[FriendshipDto]

}
