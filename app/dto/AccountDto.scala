package dto

import models.Account
import play.api.libs.json.Json

case class AccountDto(id: Long, username: String ,  var firstName: String, var lastName: String, var email: String, var phone: String, var picture: String)

  object AccountDto {
    implicit def accWriter = Json.writes[AccountDto]

    implicit def acc2AccDto(acc: Account): AccountDto =  AccountDto.this (acc.id, acc.username, acc.firstName, acc.lastName, acc.email, acc.phone, acc.picture)
  }


case class AccountSingIn( var username: String ,  var password: String )

object AccountSingIn {
  implicit def accWriter = Json.writes[AccountSingIn]
  implicit val accountFormat = Json.format[AccountSingIn]
}