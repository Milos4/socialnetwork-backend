package db

import akka.http.javadsl.model.DateTime
import models._
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.json.Json.{fromJson, toJson}
import play.api.libs.json.{Format, JsResult, JsValue}
import slick.driver.H2Driver.api._
import slick.jdbc.JdbcProfile

import java.sql.Timestamp
import java.time.{LocalDateTime, ZoneOffset}
import javax.inject.Inject
import scala.concurrent.ExecutionContext

class DatabaseSchema @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)(implicit executionContext: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile]{
  import profile.api._

  val accounts = TableQuery[Accounts]
  val posts = TableQuery[Posts]
  val likes = TableQuery[Likes]
  val friendships = TableQuery[Friendships]

  class Accounts(tag:Tag) extends Table[Account](tag,"ACCOUNTS"){

    def id = column[Int]("ID",O.PrimaryKey,O.AutoInc)
    def username = column[String]("USERNAME")
    def password = column[String]("PASSWORD")
    def firstName = column[String]("FIRSTNAME")
    def lastName = column[String]("LASTNAME")
    def email = column[String]("EMAIL")
    def phone = column[String]("PHONE")
    def picture = column[String]("PICTURE")

    def * = (id,username,password,firstName,lastName,email,phone,picture) <>((Account.apply _).tupled,Account.unapply)
  }


  /*implicit val localDateTimeMapping = MappedColumnType.base[LocalDateTime, Timestamp](
    localDateTime => Timestamp.from(localDateTime.toInstant(ZoneOffset.UTC)),
    _.toLocalDateTime
  )*/


  class Posts(tag:Tag) extends Table[Post](tag,"POSTS"){

    def id = column[Int]("ID",O.PrimaryKey,O.AutoInc)
    def accountId = column[Int]("ACCOUNT_ID")
    def content = column[String]("CONTENT")
    def time = column[Timestamp]("TIMEE")

    def account = foreignKey("FK_ACCOUNT",accountId,accounts)(_.id)

    def * =(id,accountId,content,time) <> ((Post.apply _).tupled,Post.unapply)
  }


  class Likes (tag: Tag) extends  Table[Like](tag,"LIKES"){

    def id=column[Int]("ID",O.PrimaryKey,O.AutoInc)
    def accountId=column[Int]("ACCOUNT_ID")
    def postId=column[Int]("POST_ID")

    def account = foreignKey("FK_ACCOUNT",accountId,accounts)(_.id)
    def post = foreignKey("FK_POST",postId,posts)(_.id)

    def * = (id,accountId,postId) <> ((Like.apply _).tupled,Like.unapply)
  }


  class Friendships (tag: Tag) extends  Table[Friendship](tag,"FRIENDSHIPS"){

    def id = column[Int]("ID",O.PrimaryKey,O.AutoInc)
    def senderId = column[Int]("SENDER_ID")
    def receiverId = column[Int]("RECEIVER_ID")
    def status= column[String]("STATUS")

    def sender = foreignKey("FK_SENDER",senderId,accounts)(_.id)
    def receiver = foreignKey("FK_RECEIVER",receiverId,accounts)(_.id)

    def * = (id,senderId,receiverId,status) <>((Friendship.apply _).tupled,Friendship.unapply)
  }


  val allSchemas = accounts.schema ++ posts.schema ++ likes.schema ++ friendships.schema
}
