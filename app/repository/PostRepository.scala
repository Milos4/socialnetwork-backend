package repository

import db.DatabaseSchema
import dto.PostsDto
import models.{Friendship, Post}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class PostRepository @Inject()(protected val dbConfigProvider: DatabaseConfigProvider , protected val data : DatabaseSchema)(implicit executionContext: ExecutionContext) extends HasDatabaseConfigProvider[ JdbcProfile] {


  import profile.api._

  val posts = data.posts

  def getAll() = db.run {
    posts.result
  }

  def getById(id: Int): Future[Post] = db.run {
    posts.filter(_.id === id).result.head
  }

  def getByAccountId1(id: Int): Future[Seq[Post]] = db.run {
    posts.filter(_.accountId === id).result
  }

  def getByAccountId(id: Int) = {
    val query = for {
      user <- data.accounts if (user.id === id)
      post <- posts if post.accountId === user.id
  } yield {
    (post.id, post.content, post.time, user.id, user.username, user.firstName, user.lastName,
      data.likes.filter(_.postId === post.id).length, data.likes.filter(like => like.postId === post.id && like.accountId === id).exists)
  }
  db.run(query.sortBy(_._3.desc).result)
  }


  def getPostsForAccFriends(id: Int, id2: Int) = db.run {
    posts.filter(m => (m.id === id || m.id === id2)).result
  }


  def add(post: Post) = {
    db.run(posts.insertOrUpdate(post).map(_ => post))
  }

  def update(post: Post): Future[Int] = {
    db
      .run(posts.filter(_.id === post.id)
        .map(x => (x.accountId, x.content, x.time))
        .update(post.accountId, post.content, post.time)
      )
  }

  def delete(id: Int): Future[Int] = db.run {
    posts.filter(_.id === id).delete
  }

  def getPostsByAccount(accId: Int) = {
    val query2 = for {
      friendship <- data.friendships if friendship.status === "Accepted" && (friendship.senderId === accId || friendship.receiverId === accId)
      user <- data.accounts if (!(user.id === accId) && ((user.id === friendship.senderId) || (user.id === friendship.receiverId)))
      post <- posts if post.accountId === user.id
    } yield {
      (post.id, post.content, post.time, user.id, user.username, user.firstName, user.lastName, data.likes.filter(_.postId === post.id).length, data.likes.filter(like => like.postId === post.id && like.accountId === accId).exists)
    }
    db.run(query2.sortBy(_._3.desc).result)
  }
}
