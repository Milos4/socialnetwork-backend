package repository

import db.DatabaseSchema
import models._
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class FriendshipRepository @Inject()(protected val dbConfigProvider: DatabaseConfigProvider , protected val data : DatabaseSchema)(implicit executionContext: ExecutionContext) extends HasDatabaseConfigProvider[ JdbcProfile] {


  import profile.api._
  val friendships = data.friendships

  def getAll() =db.run {
    friendships.result
  }

  def getById(id: Int) =db.run {
    friendships.filter(_.id === id).result
  }

  def getBySender(sender: Int): Future[Seq[Friendship]] = db.run {
    friendships.filter(m => (m.senderId === sender && m.status === "Accepted")).result
  }

  def getFriendshipsForAcc(acc: Int) = db.run {
    friendships.filter(m => (m.senderId === acc && m.status === "Accepted") || (m.receiverId === acc && m.status === "Accepted") ).result
  }


  def getByReceiver(receiver: Int) = {
    val query = for {
      friendship <- friendships if(friendship.receiverId === receiver && friendship.status === "Pending" )
      user <- data.accounts if user.id === friendship.senderId
    }yield{
      (friendship.id, friendship.senderId, friendship.receiverId, user.username, user.id , friendship.status)
    }
    db.run(query.result)
  }

  def exist(sender:Int, receiver: Int) = db.run{
    friendships.filter(f => (f.senderId === sender && f.receiverId===receiver) ||(f.senderId === receiver && f.receiverId===sender && f.status === "Accepted")).exists.result
  }

  def add(friendship: Friendship)= {
    db.run(friendships.insertOrUpdate(friendship).map(_ => friendship))
  }
  def update(friendship: Friendship): Future[Int] = {
    db
      .run(friendships.filter(_.id === friendship.id)
        .map(x => (x.senderId, x.receiverId,x.status))
        .update(friendship.senderId, friendship.receiverId,friendship.status)
      )
  }

  def delete(id: Int): Future[Int] = db.run {
    friendships.filter(_.id === id).delete
  }


}