package repository


import akka.http.scaladsl.model.HttpHeader.ParsingResult.Ok
import db.DatabaseSchema
import models.Account
import org.mindrot.jbcrypt.BCrypt
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.json.Json
import slick.driver.H2Driver.api._
import slick.jdbc.JdbcProfile

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class AccountRepository @Inject()(protected val dbConfigProvider: DatabaseConfigProvider , protected val data : DatabaseSchema)(implicit executionContext: ExecutionContext) extends HasDatabaseConfigProvider[ JdbcProfile] {


  import profile.api._

  val accounts = data.accounts


  def exists(name : String) : Future[Boolean]=
    db.run(accounts.filter(_.username === name).exists.result)


  def getAll(): Future[Seq[Account]] =db.run {
    accounts.result
     }

  def getById(id: Int) :Future[Account] = db.run {
    accounts.filter(_.id === id).result.head
  }

  def getByUsername(username: String) = db.run{
    accounts.filter(_.username === username).result
  }

  def add(accont: Account): Future[Account] = {
    db.run(accounts.insertOrUpdate(accont).map(_ => accont))
  }


  def update(account: Account): Future[Int] = {
    dbConfig.db
      .run(accounts.filter(_.id === account.id)
        .map(x => (x.username, x.password, x.firstName, x.lastName, x.email, x.phone, x.picture))
        .update(account.username, account.password, account.firstName, account.lastName, account.email, account.phone, account.picture)
      )
  }

  def delete(id: Int): Future[Int] = db.run {
    accounts.filter(_.id === id).delete
  }

  def validateLogin(username: String , password : String) : Future[Account]  = db.run {
    accounts.filter(acc => acc.username ===username && acc.password ===password).result.head
  }


}

