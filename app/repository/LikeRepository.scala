package repository

import db.DatabaseSchema
import models.{Like, Post}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class LikeRepository @Inject()(protected val dbConfigProvider: DatabaseConfigProvider , protected val data : DatabaseSchema)(implicit executionContext: ExecutionContext) extends HasDatabaseConfigProvider[ JdbcProfile] {


  import profile.api._
  val likes = data.likes

  def getAll() = db.run{
    likes.result
  }

  def getById(id: Int) =db.run {
    likes.filter(_.id === id).result
  }

  def getLikesByAcc(username: Int) =db.run {
    likes.filter(_.accountId === username).result
  }


  def add(like: Like)= {
    db.run(likes.insertOrUpdate(like).map(_ => like))
  }
  def update(like: Like): Future[Int] = {
    db
      .run(likes.filter(_.id === like.id)
        .map(x => (x.accountId, x.postId))
        .update(like.accountId, like.postId)
      )
  }

  def delete(id: Int): Future[Int] = db.run {
    likes.filter(_.id === id).delete
  }

  def deleteAcc( postId : Int , accId: Int ) = db.run {
    likes.filter(l => l.postId === postId && l.accountId === accId).delete
  }

}
